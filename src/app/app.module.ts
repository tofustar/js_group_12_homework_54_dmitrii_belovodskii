import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FieldComponent } from './field/field.component';
import { CellComponent } from './field/cell/cell.component';
import { TriesComponent } from './field/tries/tries.component';

@NgModule({
  declarations: [
    AppComponent,
    FieldComponent,
    CellComponent,
    TriesComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
