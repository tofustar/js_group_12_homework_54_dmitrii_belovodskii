import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent {

  hasItem = false;
 status = false;
 @Input() value = ' ';


 defineCell() {
   this.status = !this.status;
 }
}
