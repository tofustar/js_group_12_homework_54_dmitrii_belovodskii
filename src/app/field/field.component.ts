import {Component} from '@angular/core';
import {CellComponent} from "./cell/cell.component";

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.css']
})
export class FieldComponent {
  cellArray: CellComponent[] = [];
  tries = 0;

  constructor() {
    this.createTableOfCells();
  }

  createTableOfCells() {
    for (let i = 0; i < 36; i++) {
      const cell = new CellComponent ();
      this.cellArray.push(cell);
      cell.value = 'x';
    }
    const cellWithRing = this.cellArray[this.getRandom()];
    cellWithRing.hasItem = true;
    cellWithRing.value = 'o';
  }

  getRandom() {
    return Math.floor(Math.random() * this.cellArray.length);
  }

  openCell(index: number) {
    if(this.cellArray[index].hasItem) {
      console.log ('You win');

    } else {
      console.log('Away');
    }
    this.tries++;
  }

  getReset() {
    this.cellArray = [];
    this.tries = 0;
    this.createTableOfCells();
  }

}
