import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-tries',
  templateUrl: './tries.component.html',
  styleUrls: ['./tries.component.css']
})
export class TriesComponent {
  @Input() tries = 0;

}
